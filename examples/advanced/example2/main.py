from src.conveyor import Conveyor
import multiprocessing as mp


class e(object):
    def run(self, data):
        return data


buffer = 100
start_w = 1
min_w = 1
max_w = 50
threshold_up = 50
threshold_down = 100

config = {
    'nodes': {
        "e": {
            'next': [],
            'engine': e,
            'input_buffer': buffer,
            'output_buffer': buffer,
            'output': True,
            'start_workers': start_w,
            'min_workers': min_w,
            'max_workers': max_w,
            'threshold_up': threshold_up,
            'threshold_down': threshold_down,
        }
    }
}


def generate_input(c, q):
    i = 0
    while True:
        data = {'id': i, 'x': i, 'y': i}
        # print("Input: ", data)
        c.send(q, data)
        i += 1

import timeit
def print_output(c, q):

    def get_from_output():
        try:
            result = c.receive(q)
            # print("Output: ", result)
        except BaseException as err:
            print(err)

    while True:
        result = timeit.timeit(get_from_output, number=200)
        print("Result ", result)


c = conveyor = Conveyor()
c.build(config)
print("Build Done")
c.start()
print("Started Conveyor")

gen = mp.Process(target=generate_input, args=(c, 'e'))
rec = mp.Process(target=print_output, args=(c, 'e'))
gen.start()
rec.start()

