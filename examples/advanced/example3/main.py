from src.conveyor import Conveyor
import multiprocessing as mp
import time


class a(object):
    def run(self, data):
        x = data['x']
        y = data['y']
        _id = data['id']
        return {'id': _id, 'xx': x, 'yy': y}


class b(object):
    def run(self, data):
        x = data['xx']
        y = data['yy']
        _id = data['id']
        res = x + y
        return {'id': _id, 'res': res}


class c(object):
    def run(self, data):
        x = data['xx']
        y = data['yy']
        _id = data['id']
        res = x + y
        return {'id': _id, 'res': res}

class d(object):
    def __init__(self):
        self.state = {}

    def run(self, data):
        _id = data['id']
        res = data['res']
        if _id in self.state:
            return {'id': _id, 'res': res + self.state[_id]}
        else:
            self.state[_id] = res
            return {'id': _id, 'res': -1}

class e(object):
    def run(self, data):
        res = data['res']
        return res/4



buffer = 100
start_w = 15
min_w = 1
max_w = 30
threshold_up = 50
threshold_down = 100

config = {
    'nodes': {
        "a": {
            'next': ['b', 'c'],
            'engine': a,
            'input_buffer': buffer,
            'output_buffer': buffer,
            'output': False,
            'start_workers': start_w,
            'min_workers': min_w,
            'max_workers': max_w,
            'threshold_up': threshold_up,
            'threshold_down': threshold_down,

        },
        "b": {
            'next': ['d'],
            'engine': b,
            'input_buffer': buffer,
            'output_buffer': buffer,
            'output': False,
            'start_workers': start_w,
            'min_workers': min_w,
            'max_workers': max_w,
            'threshold_up': threshold_up,
            'threshold_down': threshold_down,
        },
        "c": {
            'next': ['d'],
            'engine': c,
            'input_buffer': buffer,
            'output_buffer': buffer,
            'output': False,
            'start_workers': start_w,
            'min_workers': min_w,
            'max_workers': max_w,
            'threshold_up': threshold_up,
            'threshold_down': threshold_down,
        },
        "d": {
            'next': ['e'],
            'engine': d,
            'input_buffer': buffer,
            'output_buffer': buffer,
            'output': False,
            # 'start_workers': 1,
            # 'min_workers': 1,
            # 'max_workers': 1,
            'start_workers': start_w,
            'min_workers': min_w,
            'max_workers': max_w,
            'threshold_up': threshold_up,
            'threshold_down': threshold_down,

        },
        "e": {
            'next': [],
            'engine': e,
            'input_buffer': buffer,
            'output_buffer': buffer,
            'output': True,
            'start_workers': start_w,
            'min_workers': min_w,
            'max_workers': max_w,
            'threshold_up': threshold_up,
            'threshold_down': threshold_down,
        }
    }
}


def generate_input(c, q):
    i = 0
    while True:
        data = {'id': i, 'x': i, 'y': i}
        # print("Input: ", data)
        c.send(q, data)
        i += 1
        # time.sleep(0.01)
    return

import timeit
def print_output(c, q):

    def get_from_output():
        try:
            result = c.receive(q)
            # print("Output: ", result)
        except BaseException as err:
            print(err)

    while True:
        result = timeit.timeit(get_from_output, number=150)
        print("Result ", result)
    return


c = conveyor = Conveyor()
c.build(config)
print("Build Done")
c.start()
print("Started Conveyor")

gen = mp.Process(target=generate_input, args=(c, 'a'))
rec = mp.Process(target=print_output, args=(c, 'e'))
gen.start()
rec.start()

