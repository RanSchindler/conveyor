from src.conveyor import Conveyor
import requests
import re


class c_gate(object):
    def __init__(self):
        self.id = 0

    def run(self, data):
        self.id += 1
        data['id'] = self.id
        print("Gate: ", " ", self.id, " ", data['url'])
        return data


class c_head(object):
    def run(self, data):
        try:
            r = requests.head(data['url'])
            print("HEAD: ", data['url'], " ", r.status_code)
            if r.status_code == 200:
                data['head'] = r
                return data
        except BaseException as err:
            print(err)
        return None


class c_get(object):
    def run(self, data):
        try:
            r = requests.get(data['url'])
            print("GET: ", data['url'], " ", r.status_code)
            if r.status_code == 200:
                data['get'] = r
                return data
        except BaseException as err:
            print(err)
        return None


class c_extract_href(object):
    def run(self, data):
        try:
            print("href: ", data['url'])
            text = data['get'].text
            m = re.findall(r'href=[\"\'](https?://.*?)[\"\']', text)
            data['href'] = m
            data['text'] = text
            print("href: ", m)
            return data
        except BaseException as err:
            print(err)
        return None


class c_recalc_href(object):
    def __init__(self):
        self.state = {}

    def run(self, data):
        try:
            print("Recalc: ", data['url'])
            urls = data['href']
            response = []
            for url in urls:
                if url in self.state:
                    print("Recalc url in state: ", url)
                    pass
                else:
                    self.state[url] = True
                    response.append({'url': url, 'parent': data['url']})
            print("Recalc sending to gate: ", response)
            return response
        except BaseException as err:
            print(err)
        return None

class c_text_measurements(object):
    def run(self, data):
        try:
            text = data['text']
            print("Measurements: ", data['url'], " ", text)
            return text
        except BaseException as err:
            print(err)
        return None


buffer = 100
start_w = 1
min_w = 1
max_w = 1
threshold_up = 50
threshold_down = 100

config = {
    'nodes': {
        "c_gate": {
            'next': ['c_head'],
            'engine': c_gate,
            'input_buffer': buffer,
            'output_buffer': buffer,
            'output': False,
            'start_workers': 1,
            'min_workers': 1,
            'max_workers': 1,
            'threshold_up': threshold_up,
            'threshold_down': threshold_down,

        },
        "c_head": {
            'next': ['c_get'],
            'engine': c_head,
            'input_buffer': buffer,
            'output_buffer': buffer,
            'output': False,
            'start_workers': start_w,
            'min_workers': min_w,
            'max_workers': max_w,
            'threshold_up': threshold_up,
            'threshold_down': threshold_down,
        },
        "c_get": {
            'next': ['c_extract_href'],
            'engine': c_get,
            'input_buffer': buffer,
            'output_buffer': buffer,
            'output': False,
            'start_workers': start_w,
            'min_workers': min_w,
            'max_workers': max_w,
            'threshold_up': threshold_up,
            'threshold_down': threshold_down,
        },
        "c_extract_href": {
            'next': ['c_recalc_href', 'c_text_measurements'],
            'engine': c_extract_href,
            'input_buffer': buffer,
            'output_buffer': buffer,
            'output': False,
            'start_workers': start_w,
            'min_workers': min_w,
            'max_workers': max_w,
            'threshold_up': threshold_up,
            'threshold_down': threshold_down,
        },
        "c_recalc_href": {
            'next': ['c_gate'],
            'engine': c_recalc_href,
            'input_buffer': buffer,
            'output_buffer': buffer,
            'output': False,
            'start_workers': 1,
            'min_workers': 1,
            'max_workers': 1,
            'threshold_up': threshold_up,
            'threshold_down': threshold_down,
        },
        "c_text_measurements": {
            'next': [],
            'engine': c_text_measurements,
            'input_buffer': buffer,
            'output_buffer': buffer,
            'output': True,
            'start_workers': start_w,
            'min_workers': min_w,
            'max_workers': max_w,
            'threshold_up': threshold_up,
            'threshold_down': threshold_down,
        }

    }
}


c = conveyor = Conveyor()
c.build(config)
print("Build Done")
c.start()
print("Started Conveyor")
c.send('c_gate', {'url': 'http://www.google.com'})
# output1 = c.receive('c_text_measurements')
# output2 = c.receive('c_text_measurements')
# output3 = c.receive('c_text_measurements')
# output4 = c.receive('c_text_measurements')
# output5 = c.receive('c_text_measurements')
# output6 = c.receive('c_text_measurements')
# output7 = c.receive('c_text_measurements')

# c.stop()
