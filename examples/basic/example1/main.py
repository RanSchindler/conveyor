from src.conveyor import Conveyor
import time


def a(data):
    x = data['x']
    y = data['y']
    res = x + y
    return res


config = {
    'nodes': {
        "a": {
            'next': [],
            'engine': a,
            'input_buffer': 10,
            'output_buffer': 10,
            'output': True,
        }
    }
}

c = conveyor = Conveyor()
c.build(config)
print("Build Done")
c.start()
print("Started Conveyor")
data = {'x': 10, 'y': 20}
c.send('a', data)
print("Sent Data: data = {'x': 10, 'y': 20}")
time.sleep(1)
result = c.receive('a')
print("Result: ", result)
time.sleep(1)
c.stop()
print("Stopped")
