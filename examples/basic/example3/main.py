from src.conveyor import Conveyor
import time


def a(data):
    x = data['x']
    y = data['y']
    return {'xx': x, 'yy': y}


def b(data):
    x = data['xx']
    y = data['yy']
    res = x + y
    return res

def c(data):
    x = data['xx']
    y = data['yy']
    res = x - y
    return res


config = {
    'nodes': {
        "a": {
            'next': ['b', 'c'],
            'engine': a,
            'input_buffer': 10,
            'output_buffer': 10,
            'output': False,
        },
        "b": {
            'next': [],
            'engine': b,
            'input_buffer': 10,
            'output_buffer': 10,
            'output': True,
        },
        "c": {
            'next': [],
            'engine': c,
            'input_buffer': 10,
            'output_buffer': 10,
            'output': True,
        }
    }
}

c = conveyor = Conveyor()
c.build(config)
print("Build Done")
c.start()
print("Started Conveyor")
data = {'x': 10, 'y': 20}
c.send('a', data)
print("Sent Data: data = {'x': 10, 'y': 20}")
data = {'x': 100, 'y': 200}
c.send('a', data)
print("Sent Data: data = {'x': 100, 'y': 200}")
time.sleep(1)
result = c.receive('b')
print("Result: ", result)
result = c.receive('c')
print("Result: ", result)
result = c.receive('b')
print("Result: ", result)
result = c.receive('c')
print("Result: ", result)
time.sleep(1)
c.stop()
print("Stopped")
