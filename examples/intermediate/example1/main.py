from src.conveyor import Conveyor
import time


class a(object):
    def run(self, data):
        x = data['x']
        y = data['y']
        _id = data['id']
        return {'id': _id, 'xx': x, 'yy': y}


class b(object):
    def run(self, data):
        x = data['xx']
        y = data['yy']
        _id = data['id']
        res = x + y
        return {'id': _id, 'res': res}


class c(object):
    def run(self, data):
        x = data['xx']
        y = data['yy']
        _id = data['id']
        res = x + y
        return {'id': _id, 'res': res}

class d(object):
    def __init__(self):
        self.state = {}

    def run(self, data):
        _id = data['id']
        res = data['res']

        if _id in self.state:
            return res + self.state[_id]
        else:
            self.state[_id] = res


config = {
    'nodes': {
        "a": {
            'next': ['b','c'],
            'engine': a,
            'input_buffer': 10,
            'output_buffer': 10,
            'output': False,
        },
        "b": {
            'next': ['d'],
            'engine': b,
            'input_buffer': 10,
            'output_buffer': 10,
            'output': False,
        },
        "c": {
            'next': ['d'],
            'engine': c,
            'input_buffer': 10,
            'output_buffer': 10,
            'output': False,
        },
        "d": {
            'next': [],
            'engine': d,
            'input_buffer': 10,
            'output_buffer': 10,
            'output': True,
        }
    }
}

c = conveyor = Conveyor()
c.build(config)
print("Build Done")
c.start()
print("Started Conveyor")
data = {'id': 1, 'x': 10, 'y': 20}
c.send('a', data)
print("Sent Data: data = {'x': 10, 'y': 20}")
data = {'id': 2, 'x': 100, 'y': 200}
c.send('a', data)
print("Sent Data: data = {'x': 100, 'y': 200}")
time.sleep(1)
result = c.receive('d')
print("Result: ", result)
result = c.receive('d')
print("Result: ", result)

time.sleep(1)
c.stop()
print("Stopped")
