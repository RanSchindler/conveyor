from src.conveyor import Conveyor
import time
import multiprocessing as mp
import queue


class a(object):
    def run(self, data):
        x = data['x']
        y = data['y']
        _id = data['id']
        return {'id': _id, 'xx': x, 'yy': y}


class b(object):
    def run(self, data):
        x = data['xx']
        y = data['yy']
        _id = data['id']
        res = x + y
        return {'id': _id, 'res': res}


class c(object):
    def run(self, data):
        x = data['xx']
        y = data['yy']
        _id = data['id']
        res = x + y
        return {'id': _id, 'res': res}

class d(object):
    def __init__(self):
        self.state = {}

    def run(self, data):
        _id = data['id']
        res = data['res']
        if _id in self.state:
            return (res + self.state[_id])/4
        else:
            self.state[_id] = res


config = {
    'nodes': {
        "a": {
            'next': ['b', 'c'],
            'engine': a,
            'input_buffer': 10,
            'output_buffer': 10,
            'output': False,
        },
        "b": {
            'next': ['d'],
            'engine': b,
            'input_buffer': 10,
            'output_buffer': 10,
            'output': False,
        },
        "c": {
            'next': ['d'],
            'engine': c,
            'input_buffer': 10,
            'output_buffer': 10,
            'output': False,
        },
        "d": {
            'next': [],
            'engine': d,
            'input_buffer': 10,
            'output_buffer': 10,
            'output': True,
        }
    }
}


def generate_input(c,q):
    for i in range(10):
        data = {'id': i, 'x': i, 'y': i}
        print(data)
        c.send(q, data)
    print("Done sending")
    return


def print_output(c,q):
    for i in range(10):
        result = c.receive(q)
        print("Result ", result)
    print("Done receiving ")
    return

c = conveyor = Conveyor()
c.build(config)
print("Build Done")
c.start()
print("Started Conveyor")

gen = mp.Process(target=generate_input, args=(c, 'a'))
rec = mp.Process(target=print_output, args=(c, 'd'))
gen.start()
rec.start()
gen.join()
rec.join()
print("send STOP")
c.stop()
print("Stopped")
