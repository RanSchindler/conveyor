import multiprocessing as mp
import time
import queue
import inspect


class CallableEngine(object):
    def __init__(self, f):
        self.f = f

    def run(self, data):
        return self.f(data)


def node_worker(node_data):
    try:
        node = node_data['node']
        sphare = node_data['sphare']

        # print(node_data['id'], " node_worker node: ", node)
        # print(node_data['id'], " node_worker sphare: ", sphare)

        f = None
        if inspect.isclass(node['engine']):
            f = node['engine']()
        elif callable(node['engine']):
            f = CallableEngine(node['engine'])
        else:
            print(node_data['id'], " ERROR: Couldn't wrap engine")

        in_q = sphare['listen_q']
        out_q = sphare['output_q']
        speak_q = sphare['speak_q']
        manager_q = sphare['manager_q']

        while True:
            try:
                next_input = in_q.get_nowait()
            except queue.Empty:
                next_input = None

            if next_input:
                # ENGINE
                try:
                    result = f.run(next_input)
                except Exception as err:
                    result = None
                    print(node_data['id'], " node_worker: Couldn't run engine ", err)
                # ## ### ### ### ### ### ### ###

                # Publish results ### ### ###
                if result is not None:
                    print(result)
                    if not isinstance(result, (list, tuple)):
                        result = [result]
                    for res in result:
                        if out_q:
                            out_q.put(res)
                        for q in speak_q:
                            q.put(res)
                # ## ### ### ### ### ### ### ###

            try:
                msg = manager_q.get_nowait()
            except queue.Empty:
                msg = None
            if msg:
                if msg == 'STOP':
                    print(node_data['id'], " node_worker: Bye :)")
                    return

            time.sleep(0.1)

    except BaseException as err:
        print("Exception:", err)


def conveyor_manager(data):
    try:
        config = data['config']
        sphare = data['sphare']
        manager_q = data['manager_q']

        print("conveyor_manager Config: ", config)
        print("conveyor_manager Sphare: ", sphare)

        # Init workers
        workers = {}
        dynamic = {}
        for node in sphare:
            workers[node] = []
            dynamic[node] = {True: 0, False: 0}

        for node in sphare:
            node_data = {
                'node': config['nodes'][node],
                'sphare': sphare[node],
                'id': node
            }

            if 'min_workers' in config['nodes'][node]:
                start_workers = config['nodes'][node]['start_workers']
                ps = [mp.Process(target=node_worker, args=(node_data,)) for _ in range(start_workers)]
                workers[node].extend(ps)
                _ = [p.start() for p in ps]
            else:
                p = mp.Process(target=node_worker, args=(node_data,))
                workers[node].append(p)
                p.start()

        while True:
            for node in workers:
                for i, p in enumerate(workers[node]):
                    if p is not None and not p.is_alive():
                        workers[node][i] = None
                        workers[node] = [p for p in workers[node] if p is not None]

            for node in config['nodes']:
                if 'min_workers' in config['nodes'][node] and 'max_workers' in config['nodes'][node] and \
                 'threshold_up' in config['nodes'][node] and 'threshold_down' in config['nodes'][node]:
                    min_w = config['nodes'][node]['min_workers']
                    max_w = config['nodes'][node]['max_workers']
                    threshold_up = config['nodes'][node]['threshold_up']
                    threshold_down = config['nodes'][node]['threshold_down']
                    current_workers = len([p for p in workers[node] if p is not None])
                    current_input_q_full = sphare[node]['listen_q'].full()

                    dynamic[node][current_input_q_full] += 1

                    if dynamic[node][True] > threshold_up:
                        dynamic[node] = {True: 0, False: 0}

                        if current_workers < max_w:
                            node_data = {
                                'node': config['nodes'][node],
                                'sphare': sphare[node],
                                'id': node
                            }
                            p = mp.Process(target=node_worker, args=(node_data,))
                            workers[node].append(p)
                            p.start()
                    if dynamic[node][False] > threshold_down:
                        if current_workers > min_w:
                            sphare[node]['manager_q'].put("STOP")
                            dynamic[node] = {True: 0, False: 0}

            try:
                msg = manager_q.get_nowait()
            except queue.Empty:
                msg = None

            if msg:
                if msg == 'STOP':
                    for node in workers:
                        for _ in workers[node]:
                            sphare[node]['manager_q'].put("STOP")
                    time.sleep(3)
                    for node in workers:
                        for p in workers[node]:
                            p.terminate()
                    print("conveyor_manager: Bye :)")
                    return

            time.sleep(0.1)

    except BaseException as err:
        print("Exception:", err)


class Conveyor(object):
    def __init__(self):
        self.nodes_sphare = None
        self.config = None
        self.p_manager = None
        self.manager_q = None

    def build(self, config):
        self.nodes_sphare = {}
        self.config = config
        nodes = config['nodes']
        for node in nodes:
            print("Node: ", node, " ", nodes[node])
            self.nodes_sphare[node] = {
                'listen_q': mp.Queue(nodes[node]['input_buffer']),
                'output_q': mp.Queue(nodes[node]['output_buffer']) if nodes[node]['output'] is True else None,
                'speak_q': [],
                'manager_q': mp.Queue()
            }

        for node in nodes:
            for next_node in nodes[node]['next']:
                if next_node in self.nodes_sphare:
                    self.nodes_sphare[node]['speak_q'].append(self.nodes_sphare[next_node]['listen_q'])
                else:
                    print(f"Conveyor : build : Couldn't connect node {node} to node {next_node}")

    def start(self):
        self.manager_q = mp.Queue()
        data = {
            'config': self.config,
            'sphare': self.nodes_sphare,
            'manager_q': self.manager_q
        }
        self.p_manager = mp.Process(target=conveyor_manager, args=(data,))
        self.p_manager.start()

    def stop(self):
        self.manager_q.put("STOP")
        self.p_manager.join()
        return

    def send(self, q, data):
        if q in self.nodes_sphare and 'listen_q' in self.nodes_sphare[q]:
            self.nodes_sphare[q]['listen_q'].put(data)

    def receive(self, q):
        if q in self.nodes_sphare and 'output_q' in self.nodes_sphare[q]:
            return self.nodes_sphare[q]['output_q'].get()


'''
config = {
    'nodes': {
        "d": {
            'next': [],
            'engine': d,
            'input_buffer': 10,
            'output_buffer': 10,
            'output': True,
            'start_workers': 2,
            'min_workers': 1,
            'max_workers': 3,
            'threshold_up': 100,
            'threshold_down': 500,
        }
    }
}
'''
